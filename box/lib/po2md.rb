# encoding: UTF-8
# coding: UTF-8

require __dir__ + '/common'

class PO2MD
  def initialize input
    @name = File.basename(input, '.*')
    @md   = []

    get_po(input)[1..-1].each do |e|
      if e[:msgstr] then @md.push(e[:msgstr]) end
    end
  end

  def save out = nil
    out = out ? File.absolute_path(out) : Dir.pwd + '/' + @name + '.md'

    file = File.open(out, 'w:utf-8')
    file.puts @md.join("\n\n")
    file.close
  end
end
