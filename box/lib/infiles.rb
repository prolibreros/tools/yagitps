# encoding: UTF-8
# coding: UTF-8

require 'fileutils'
require 'digest/sha1'
require __dir__ + '/common'

class InFiles

  def initialize type
    @files_awake = []
    @files_sleep = []

    path = get_root + "in/txt/#{type}/**/*.#{type}"

    Dir.glob(path).each do |base_file|
      sha1_new = Digest::SHA1.file base_file
      sha1_old = get_sha1(sha1_new) # Tries to retrieves with same sha1

      # Awakes if:
      # 1. sha1 file doesn't exists == new file generated
      # 2. sha1sums aren't the same == file has changed
      if sha1_old == nil || sha1_new != sha1_old[:sha1]
        gen_sha1_file(sha1_new, base_file)
        if sha1_old
          FileUtils.rm(sha1_old[:path])
        end
        @files_awake.push(base_file)
      else
        @files_sleep.push(base_file)
      end
    end
  end

  def awake?
    return @files_awake.length > 0 ? true : false
  end

  def awake
    return @files_awake
  end

  def sleep
    return @files_sleep
  end
end
